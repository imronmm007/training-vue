import axios from "axios";

export default {
  state: {},
  getters: {},
  mutations: {},
  actions: {
    LOGIN: ({}, payload) => {
      return new Promise((resolve, reject) => {
        axios
          .post("home/login", payload, {
            withCredentials: true
          })
          .then(({
            data,
            status
          }) => {
            if (status === 200) {
              if (data.Msg == null) {
                const token = data.Data.Token;
                const username = data.Data.UserName;
                const role = data.Data.UserGroup;
                const asset = data.Data.Asset;
                const longname = data.Data.LongName;
                localStorage.setItem("access_token", token);
                localStorage.setItem("username", username);
                localStorage.setItem("role", role);
                localStorage.setItem("asset", asset);
                localStorage.setItem("longname", longname);
                resolve(true);
              } else {
                reject(data.Msg);
              }
            }

            if (status === 401) {
              localStorage.removeItem("access_token");
              localStorage.removeItem("username");
              localStorage.removeItem("role");
              localStorage.removeItem("asset");
              localStorage.removeItem("longname");
              resolve(true);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },

    LOGOUT: () => {
      return new Promise(resolve => {
        localStorage.removeItem("access_token");
        localStorage.removeItem("username");
        localStorage.removeItem("role");
        localStorage.removeItem("asset");
        localStorage.removeItem("longname");
        resolve();
      });
    },

  }
};
