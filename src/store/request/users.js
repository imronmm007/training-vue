import { Get, Post, Delete } from "./index";

export default {
  actions: {
    GET_USERS: () => {
      return new Promise((resolve, reject) => {
        function callback(res) {
          if (res.status === 200) {
            resolve(res.data.Data);
          }
        }
        Get("values/Users", reject, callback);
      });
    },

    GET_USERS_DETAIL: (_, payload) => {
      return new Promise((resolve, reject) => {
        function callback(res) {
          if (res.status === 200) {
            resolve(res.data.Data);
          }
        }
        Get("values/Users/" + payload, reject, callback);
      });
    },

    POST_INSERT: (_, payload) => {
      return new Promise((resolve, reject) => {
        function callback(res) {
          if (res.status === 200) {
            resolve(res.data.Data);
          }
        }
        Post("values/Users", payload, reject, callback);
      });
    },

    DELETE_USER: (_, payload) => {
      return new Promise((resolve, reject) => {
        function callback(res) {
          if (res.status === 200) {
            resolve(res.data.Data);
          }
        }
        Delete("values/Users/" + payload, reject, callback);
      });
    }
  }
};
