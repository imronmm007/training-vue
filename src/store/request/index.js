import axios from "axios";
import decode from "jwt-decode";

const AUTH_TOKEN_KEY = "access_token";

function HeaderAuth() {
  return {
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("access_token")
    }
  };
}

function forceLogout() {
  localStorage.clear();
  window.location.href = "/";
}

export function isLoggedIn() {
  let authToken = localStorage.getItem(AUTH_TOKEN_KEY);
  return !!authToken && !isTokenExpired(authToken);
}

function isTokenExpired(token) {
  let expirationDate = getTokenExpirationDate(token);
  return expirationDate < new Date();
}

function getTokenExpirationDate(encodedToken) {
  let token = decode(encodedToken);
  if (!token.exp) {
    return null;
  }

  let date = new Date(0);
  date.setUTCSeconds(token.exp);

  return date;
}

function Catch(error, reject) {
  if (typeof error.response === "undefined") {
    reject("Network Error, Connection Not Found");
  } else {
    if (error.response.status === 422 || error.response.status === 500) {
      reject(error.response.data.message);
    } else if (error.response.status === 401) {
      forceLogout();
    }
  }
}

export function Get(url, reject, callback) {
  let config = HeaderAuth();
  return axios
    .get(url, config)
    .then(res => {
      callback(res);
    })
    .catch(error => {
      Catch(error, reject);
    });
}

export function Post(url, payload, reject, callback) {
  return axios
    .post(url, payload, HeaderAuth())
    .then(res => {
      callback(res);
    })
    .catch(error => {
      Catch(error, reject);
    });
}

export function Delete(url, reject, callback) {
  return axios
    .delete(url, HeaderAuth())
    .then(res => {
      callback(res);
    })
    .catch(error => {
      Catch(error, reject);
    });
}
